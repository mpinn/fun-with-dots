ArrayList<Dot> dots; 
int maxDots = 50;   

boolean graphicsLoaded = false;

void setup() {
  size(640, 530); 
  smooth(8);
  thread("loadGraphics"); 
}

void draw() {
  if (graphicsLoaded) {
    background(0);    
    ellipseMode(CENTER);

    for (Dot dot: dots) { 
      dot.display(); 
    }
  }
}

void loadGraphics() {
  dots = new ArrayList<Dot>();
  
  for (int i = 0; i < maxDots; i++) { 
    dots.add(new Dot()); 
  }
  graphicsLoaded = true; 
}

class Dot {
  PVector location; 
  PVector randomLocation; 
  float acceleration; 
  PVector dir; 
  int dotWidth, dotHeight; 
  color lineColor; 
  int detection;

  Dot() {
    location = new PVector(random(0, width), random(0, height));
    randomLocation = new PVector(random(0, width), random(0, height)); 
    acceleration = random(1, 2); 
    dir = new PVector(0, 0); 
    dotWidth = 1; 
    dotHeight = 1;
    detection = 100;
    lineColor = color(255);
  }

  void display() {
    drawDot();
  }

  void drawDot() {
    PVector desired = new PVector(randomLocation.x, randomLocation.y);
    float d = PVector.dist(location, desired);

    if (d <= dotWidth+1) {
      generateRandomLocation();
    }
    else {
      dir = PVector.sub(desired, location);
      dir.normalize();
      dir.mult(acceleration);
      location.add(dir);
    }

    fill(0, 0, 255);
    stroke(0);
    ellipse(location.x, location.y, dotWidth, dotHeight);
    
    for(Dot dot: dots){
      float d2 = PVector.dist(location, dot.location);
      if(d2 < dotWidth+detection){
        stroke(detection-d2, detection-d2, 255-d2); //Makes a fading effect (Cleaner Design).
        line(location.x, location.y, dot.location.x, dot.location.y); 
      }     
    }
  }

  void generateRandomLocation() {
    randomLocation = new PVector(random(0, width), random(0, height));
  }
}

